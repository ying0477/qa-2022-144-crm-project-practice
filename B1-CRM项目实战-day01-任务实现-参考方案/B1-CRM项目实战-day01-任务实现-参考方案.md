# 实战任务-参考答案





# 3-1-阅读产品文档

无论任何项目产品，什么技术，都是为了解决一定的需求，离开需求谈技术没有意义



**确定CRM系统是什么？**

![image-20220610171252387](assets/image-20220610171252387.png)





#### 获取产品需求的途径：

​	1）与产品经理沟通（最简单最直接的方式）

​	2）阅读产品文档 （最具权威的方式，按照产品文档里的描述进行开发一定没问题，就算有问题也不是开发的问题）

#### 思路：

​	1.需要阅读产品原型,线上地址https://app.mockplus.cn/s/hvKXEoWW3g2l

​	2.阅读学员任务资料\任务3-阅读产品文档\其它产品资料里的内容

​	3.思考系统中有哪些角色，哪些核心业务模块，分别有什么用？

#### 问题检验：

- ​	你觉得汇客CRM系统中有哪些核心模块

- ​	你觉得汇客CRM系统中的线索，商机，合同分别是什么

- ​	你觉得线索，商机，合同的关系是什么，什么时候线索转换成合同经历了哪些步骤

- ​	你觉得汇客CRM系统中的线索池，公海池里存储的是什么？这部分的数据是怎么来的

- ​	你觉得汇客CRM系统主要是给什么样的人员来使用的？任何人都能使用吗？

- ​	你觉得使用汇客CRM系统的公司主要销售的产品是什么


#### 实现思路：

CRM专业术语解释

```
线索：销售线索是与客户初次接触获得的原始信息，可以是从民会中获得的名片，通过推广活动获得的毛话号码，或是会议、广告、外部购买等渠道获得的客户简单信息，然后通过管理和跟进可以转化为商机
线索池：跟进后，没有转化成商机的线索集合，可以理解为线索回收站
商机：商机是从意向客户到成交客戶的跟进过程，最后的通过签订合同，转变水成交客戶
公海池：己跟进，但未成功转化的潜在客户，可以理解为潜在客户的回收站
客广：本系统内的客户是指成功签订合同的客户，由商机转化而来
合同：合同是销售过程中的一个重要组成部分，表示客户己成交的矣键步骤。
转派：系统中用户离职，产生的线索、商机的重新分配任买
```



通过阅读线上的产品原型我们可以看到系统中具有的核心模块

![](assets/777402fa5aa24eb859dbb15d0ec878d.png)

核心模块有线索管理，商机管理，合同管理，转派管理，系统管理

其中线索管理主要的功能是：查看线索相关的信息，分页展示所有的线索，可以进行线索的查看，分配，线索的创建和导入等

![](assets/f9ee34cc187b08d9cd103b6b699d71e.png)

可以看到线索的数据来源有两个一个是人工录入即添加线索的方式，还有一个就是批量导入的方式

查看按钮可以查看具体的线索的内容

商机管理：

![](assets/660a98ca979a66fdd20e46040a5396e.png)

通过产品原型可以看到商机相关的信息，分页展示所有的商机，可以进行商机的查看，分配，商机的创建

合同管理：

![](assets/766504d0f1ef5b044e00c937ee819cc.png)

合同管理可以看到合同相关的信息，分页展示所有的合同，可以进行合同的查看和添加





CRM系统主要是辅助企业的销售人员继续客户的筛选和沟通的

做活动的人员进行线下的地推，线上的活动收集到很多的感兴趣的客户，录入到CRM系统中

公司的线索专员基于线下收到的信息与客户进行沟通，这个过程叫跟进，如果客户足够有意向想要了解，线索专员将其转换成商机，由专业的商机专员进行跟进，如果客户确定要学习课程，与客户签订合同后商机专员将其转换成合同



你觉得使用汇客CRM系统的公司主要销售的产品是什么 

通过产品原型可以了解到销售的是课程

![](assets/b0aeb4396cea150b4e3a01740979543.png)

























# 4-1-看代码（实现）

### 1.看什么？哪些是重点？哪些是非重点？如何区分

​	**代码层面**

​	**返回的对象的封装类是什么**

​		系统提供了：	AjaxResult和TableDataInfo两个封装类



#### 	**1.1 其中AjaxResult主要是用来封装一些非分页对象的**

​		AjaxResult具有三个属性：code msg data

​		其中提供了很多方法用来描述接口调用成功还是失败

```java
package com.huike.common.core.domain;

import java.util.HashMap;
import com.huike.common.constant.HttpStatus;
import com.huike.common.utils.StringUtils;

/**
 * 操作消息提醒
 * 
 * 
 */
public class AjaxResult extends HashMap<String, Object>
{
    private static final long serialVersionUID = 1L;

    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MSG_TAG = "msg";

    /** 数据对象 */
    public static final String DATA_TAG = "data";

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public AjaxResult()
    {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     * 
     * @param code 状态码
     * @param msg 返回内容
     */
    public AjaxResult(int code, String msg)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    public AjaxResult(int code, String msg, Object data)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (StringUtils.isNotNull(data))
        {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public static AjaxResult success()
    {
        return AjaxResult.success("操作成功");
    }

    /**
     * 返回成功数据
     * 
     * @return 成功消息
     */
    public static AjaxResult success(Object data)
    {
        return AjaxResult.success("操作成功", data);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @return 成功消息
     */
    public static AjaxResult success(String msg)
    {
        return AjaxResult.success(msg, null);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static AjaxResult success(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @return
     */
    public static AjaxResult error()
    {
        return AjaxResult.error("操作失败");
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @return 警告消息
     */
    public static AjaxResult error(String msg)
    {
        return AjaxResult.error(msg, null);
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static AjaxResult error(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.ERROR, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @return 警告消息
     */
    public static AjaxResult error(int code, String msg)
    {
        return new AjaxResult(code, msg, null);
    }
}
```

​		



#### **1.2 其中TableDataInfo主要是用来返回分页的数据的**

TableDataInfo包含了total 总条数 rows列表数据 code 状态码 msg消息内容 params(其他参数)

```java
package com.huike.common.core.page;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 表格分页数据对象
 * 
 * 
 */
public class TableDataInfo implements Serializable{
	
    private static final long serialVersionUID = 1L;

    /** 总记录数 */
    private long total;

    /** 列表数据 */
    private List<?> rows;

    /** 消息状态码 */
    private int code;

    /** 消息内容 */
    private String msg;

    private Map<String,Object> params;

    /**
     * 表格数据对象
     */
    public TableDataInfo()
    {
    }

    /**
     * 分页
     * 
     * @param list 列表数据
     * @param total 总记录数
     */
    public TableDataInfo(List<?> list, int total)
    {
        this.rows = list;
        this.total = total;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public List<?> getRows()
    {
        return rows;
    }

    public void setRows(List<?> rows)
    {
        this.rows = rows;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }
}

```

​	代码中使用的时候将所有分页的方法都写在了BaseController中，在BaseController中有很多获取TableDataInfo的方法

```java
package com.huike.common.core.controller;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huike.common.constant.HttpStatus;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.PageDomain;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.core.page.TableSupport;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.StringUtils;
import com.huike.common.utils.sql.SqlUtil;

/**
 * web层通用数据处理
 * 
 * 
 */
public class BaseController
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage()
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize))
        {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected TableDataInfo getDataTable(List<?> list){
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }


    /**
     * 手工分页
     * @param list
     * @return
     */
    public TableDataInfo getDataTablePage(List<?> list) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        int total = list.size();
        if (total > pageSize) {
            int toIndex = pageSize * pageNum;
            if (toIndex > total) {
                toIndex = total;
            }
            list = list.subList(pageSize * (pageNum - 1), toIndex);
        }
        rspData.setRows(list);
        rspData.setTotal(total);
        return rspData;
    }

    protected TableDataInfo getDataTable(List<?> list, Map<String,Object> params)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        rspData.setParams(params);
        return rspData;
    }

    protected TableDataInfo getDataTablePage(List<?> list, Map<String,Object> params)
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        int total = list.size();
        if (total > pageSize) {
            int toIndex = pageSize * pageNum;
            if (toIndex > total) {
                toIndex = total;
            }
            list = list.subList(pageSize * (pageNum - 1), toIndex);
        }
        rspData.setRows(list);
        rspData.setTotal(total);
        rspData.setParams(params);
        return rspData;
    }


    /**
     * 响应返回结果
     * 
     * @param rows 影响行数
     * @return 操作结果
     */
    protected AjaxResult toAjax(int rows)
    {
        return rows > 0 ? AjaxResult.success() : AjaxResult.error();
    }

    /**
     * 响应返回结果
     * 
     * @param result 结果
     * @return 操作结果
     */
    protected AjaxResult toAjax(boolean result)
    {
        return result ? success() : error();
    }

    /**
     * 返回成功
     */
    public AjaxResult success()
    {
        return AjaxResult.success();
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error()
    {
        return AjaxResult.error();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(String message)
    {
        return AjaxResult.success(message);
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error(String message)
    {
        return AjaxResult.error(message);
    }

    /**
     * 页面跳转
     */
    public String redirect(String url)
    {
        return StringUtils.format("redirect:{}", url);
    }
}

```



#### **1.3 架构层面**	![](assets/crm-framework.png)



#### **1.4 三层架构是如何编写的**

​		controller层  主要写在admin工程中的controller包下

​		service层主要写在各个业务工程的service包下 service目录下是接口 impl包下是具体的实现类

​		mapper层主要写在各个业务工程下的mapper目录下 mapper目录下是接口

​			对应的xml文件是在各个模块的resource目录下有具体的xml文件

​			xml与mapper接口的对应是写在xml文件中对应了mapper接口的全路径

​			如图所示：![](assets/a01daa13de9d5522e3b069364e11c70.png)



#### **1.5 业务中数据的状态有哪些，是写在哪里的**

​		线索状态写在了线索的实体类中的一个枚举类中

```java
 public enum StatusType{


        UNFOLLOWED("待跟进","1"),
        FOLLOWING("跟进中","2"),
        RECOVERY("回收","3"),
        FALSE("伪线索/踢回公海","4"),
        DELETED("删除","5"),
        TOBUSINESS("转换商机","6"),
        TOCUSTOMER("转换客户","7");

        private String name;
        private String value;

        private StatusType(String name,String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
```

​		商机集成了线索，状态与线索的状态相同

```java
public class TbBusiness extends TbClue{
```



#### 	**1.6 数据库层面**

​	![](assets/478049b057b3e6282709420386fa433.png)**

​	**项目结构层面**

​		各个模块对应的职责是什么

​		![](assets/eeb6b800660be65ba28a994ddb9fa6f.png)

​		admin工程主要提供的是程序的入口即controller和springboot的启动类

​		![](assets/b0cbb3c34fc419fada548e05fcdb9a0.png)

​		其他的工程主要是对应功能模块的业务层和数据持久层和一些对应模块的封装类，工具类等等

​		如商机和线索模块

​		![](assets/634da4615c37c9d4062dda0061d298b.png)





### 2.如何高效的看代码（idea标签）

**标记书签**

ctrl+f11 后按 0~9 a~z 提供了这么多的书签

设置0号标签  ctrl+f11后按0

**快速跳转到标签**

假设之前设置了0号标签

跳转到0号标签 ctrl+0

**刪除标签**

选择标签所在行按F11









# 4-2 验证码

**在看验证码这部分代码的时候需要带着一些问题来看代码**

- **1.验证码是什么时候生成的？**
- **2.验证码的有效期是如何实现的？**
- **3.验证码是如何返回给前端的？**

- **4.登录时验证码是如何比对的？**

- **5.登录一般都会使用到缓存，我们的系统中是否有使用到缓存，用的是什么缓存，存了什么？**



### **思路**

1.验证码是实在首页登录的时候使用的，在首页初使用浏览器F12查看前端是如何调用验证码接口的

但是目前我们还没有启动代码，可以通过传智提供的在线体验项目来查看

http://huike-crm.itheima.net/#/login

2.对应的在后端找到对应后端代码，逐行跟进对应的工具包里的代码的实现逻辑不用深挖



### **操作步骤：**

1.确定接口位置，验证码在浏览前端页面的时候可以看到在登录处有显示一个验证码

![](assets/770c69b193526e0cf7caf70588291d1.png)

通过浏览器F12，可以定位前端调用了那个接口

![](assets/02ba6844938ace4404dd94c647b426a.png)

可以看到调用的接口是captchaImage

使用全局搜索ctrl+shift+r搜索/captchaImage

![](assets/5cc2882ebfe65c902325ff649c53bd8.png)

直接定位到接口位置

CaptchaController

```java
package com.huike.web.controller.common;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.code.kaptcha.Producer;
import com.huike.common.constant.Constants;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.redis.RedisCache;
import com.huike.common.utils.sign.Base64;
import com.huike.common.utils.uuid.IdUtils;

/**
 * 验证码操作处理
 * 
 * 
 */
//@Api("验证码")
@RestController
public class CaptchaController {
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisCache redisCache;
    
    // 验证码类型
    @Value("${huike.captchaType}")
    private String captchaType;

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public AjaxResult getCode(HttpServletResponse response) throws IOException{
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        // 生成验证码
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }

        AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
    }
}

```

具体流程如下图所示：
![](assets/20220313115319.png)

具体获取验证码流程：

1.前端触发调用验证码接口获取验证码/captchaImage。

2.由于后端提供了具体的controller，并且提供了对应的验证码接口，后端能够被触发

```java
 @RestController

public class CaptchaController {

/**
 * 生成验证码
 */
@GetMapping("/captchaImage")
public AjaxResult getCode(HttpServletResponse response) throws IOException{
```

开始执行生成验证码的逻辑

3.验证码生成，使用了谷歌开源的工具包kaptcha

生成的数字验证码的代码，并不需要我们过多关心，我们只需要调用api即可，kaptcha可以生成数字或字符串的不用的验证码，系统中使用配置文件的方式进行配置

通过@Value注解的方式读取application.yml里的配置信息

```java
    @Value("${huike.captchaType}")
    private String captchaType;
```

![](assets/26fe64d73a1e2e53848ca51a55b0ff2.png)

那么这里读取的配置就是配置文件里配置的字符串math

具体执行的代码

```java
@Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisCache redisCache;
    
    // 验证码类型
    @Value("${huike.captchaType}")
    private String captchaType;

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public AjaxResult getCode(HttpServletResponse response) throws IOException{
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        // 生成验证码
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }

        AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
    }
```

第一遍看代码的时候，遇到自己不明白的地方先跳过

```java
 @GetMapping("/captchaImage")
    public AjaxResult getCode(HttpServletResponse response) throws IOException{
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;
```

这里初始化了一堆东西，是什么，有什么用，现在知不知道，不知道？

怎么办？先跳过，反正就是生成一个一堆字符串呗，也不知道干了什么

未来读代码的时候也是这样遇到不明白的部分就先跳过，等用到的时候再来研究

```java
		// 生成验证码
        if ("math".equals(captchaType)){
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }else if ("char".equals(captchaType)){
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }
```

再来看这部分代码

```java
if ("math".equals(captchaType)){
	xxx
}else if ("char".equals(captchaType)){
	xxx
}
这部分是什么意思？能不能看明白
```

判断配置文件里的信息，captchaType现在在配置文件里配置的是什么？

![](assets/26fe64d73a1e2e53848ca51a55b0ff2.png)

那么代码会走哪部分

```java
if ("math".equals(captchaType)){
	xxx
}else if ("char".equals(captchaType)){
	xxx
}
```

一定是上半部分 math的部分

在这里面又做了什么

```java
		   String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
```

这里就有几个问题了captchaProducerMath是什么？

capStr，code，image都是什么，一开始定义了什么？

substing是截取字符串，为什么要截取字符串？

我们回头来看定义的部分

```java
String capStr = null, code = null;
BufferedImage image = null;
if ("math".equals(captchaType)){
      String capText = captchaProducerMath.createText();
      capStr = capText.substring(0, capText.lastIndexOf("@"));
      code = capText.substring(capText.lastIndexOf("@") + 1);
      image = captchaProducerMath.createImage(capStr);
}
```

也就是说capStr，code，image都是提前定义好的，一开始都是null

我们可以打断点debug的方式来看具体都是什么内容，为什么要截取字符串

![](assets/847e7372c5c374da99eab284481442c.png)

打上三个断点然后debug启动

前端刷新重新获取验证码，可以看到进入断点

![](assets/f01f2cd72c8272dd8745199823553a5.png)

可以看到capText的内容是7*0=?@0

按照@来截取字符串  capStr是7*0=?   code的内容是0

这是什么？code是计算的结果capStr是公式

这个过程具体是怎么生成的我们需要管吗 这都是 String capText = captchaProducerMath.createText();替我们生成好的 而 captchaProducerMath 正是调用了google的kaptcha的工具包所以具体它是如何生成的我们并不需要关心

我们通过截取字符串的形式截取出验证码的结果code和验证码的公式capStr

但是我们返回给前端的不是一个公式，而是一个图片验证码，这个还需要调用captchaProducerMath的createImage方法将公式传入，工具包会给我们生成一个图片，通过BufferedImage来接收

![](assets/da60d21f51efc15342cb3032343ddd3.png)

将其生成具体的图片后保存在内存中



现在继续思考，我们在内存中有了图片了，

1）我不能把内存中的信息返回给前端，我应该怎么将图片返回给前端呢

2）现在后端生成了验证码的结果，内存中也存储了图片了，后续将图片返回给前端了，在登录的时候如何比对这个验证码的结果呢？ 比如现在张三获取了验证码1 ，李四获取了验证码2 ，这两个人都拿到了验证码，我如何区分这两个人呢？总不能张三提交的验证码和李四生成的验证码进行对比把

3）我们用过别人的系统，验证码都有超时时间，现在还没看到超时时间如何设置

带着这两个问题我们继续来看代码

```java
        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }

        AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
```

这里我们看到代码做了什么事，先不用思考为什么，和具体做了什么？

```java
redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
```

往redis里存了一堆东西

```java
		// 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }
```

操作数据流，不知道做了什么 但是看到一个jpg似乎是个图片

```java
 	    AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
```

这里我知道 构建了一个结果集AjaxResult对象返回 并往这个对象里设置了一些属性 uuid 和 img不知道是什么

我们一个一个来分析

```
redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
```

redisCache.setCacheObject是什么意思往redis里添加属性，redis里添加属性一般都是key,value形式的数据

那么verifyKey就是redis的key，

code在redis中是value ，存的是验证码的值

那么我们回头来看verifyKey是怎么生成的

```java
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
```

String uuid = IdUtils.simpleUUID();

这就说明这是一个简易的id生成工具类，有兴趣的同学可以了解一下什么是uuid

然后定义了一个verifyKey是一个常量+刚刚生成的id组合而来

然后作为一个redis的key存入到redis中 

存储redis的时候还设置了一个redis的超时时间

![](assets/59ca85bbe5cd10fbd709fbd9433a48c.png)

这样我们就往redis里存了验证码的结果

具体怎么存的呢：定义了一个id作为Key,value是验证码的值，并且设置了一个过期时间是2分钟

这样有什么意义呢？先不管，反正先这样往redis里存储了一份

然后继续看代码

```java
		// 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }
```

定义了一个outputStream这是一个输出流

然后将图片里的信息写入到输出流里，那么这个输出流怎么用不知道

继续看代码

```java
		AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
```

在构建结果集的时候这里使用了Base64转码，os是输出流，将输出流里的信息进行base64位转码这是要做什么

这是一种文件的传输方式，后端用base64的方式进行转码，那么前端也可以用base64的方式转回来，我们给前端传的是图片，前端转回来也是图片

那么后端图片是如何传递给前端的呢？将图片先通过工具构建出来在内存中，并将其通过base64进行转码，然后通过包装类返回给前端

然后我们来看这个包装类里还有什么，将我们一开始生成的一个id也封装进去了，尝试思考，为什么要有这个id，有什么用？ 当然如果想不明白也没有关系，我们可以先跳过这个问题，但是大概的关于验证码生成这部分的逻辑就已经全部讲完了，我们总结一下

步骤：

​	1.前端调用接口

​	2.后端读取配置文件，基于配置文件里的信息生成验证码

​	3.生成了验证码以后，将验证码的结果保存在redis中，并设置超时时间2分钟，其中key为一个id，值为验证码的值

​	4.封装结果集返回前端，返回我们生成的一个id，和base64转码后的图片

如果现在你看到这里还有不明白的点，可能还有超时时间是如何设置的？，这个id是什么？为什么要存在redis中等问题，我们需要接下去看登录部分。到此验证码生成部分已经全部讲完了

​	







# 4-3-jwt实现

**在了解jwt的时候先要带着一部分问题**

**1.什么是JWT**

**2.jwt有什么好处，对比之前学过的session**

**3.jwt的组成**

**4.如何使用JWT**

**5.系统中的JWT是如何使用的在什么时候使用的**





**思路**

1.百度了解JWT先学习技术

2.自己尝试做一个工程，引入jwt，并使用生成token

3.对应的跟进jwt的特性，分析系统中哪部分使用了jwt，是如何使用的？可以使用全局搜索ctrl+shift+r

搜索"登录"或"/login"

### 什么是JWT？(自行百度)

json web token，通过数字签名的方式，以json为载体，在不同的服务之间安全的传输信息的一种技术

### JWT有什么用？(自行百度)

一般使用在授权认证的过程中，一旦用户登录，后端返回一个token给前端，相当于后端给了前端返回了一个授权码，之后前端向后端发送的每一个请求都需要包含这个token，后端在执行方法前会校验这个token（安全校验），校验通过才执行具体的业务逻辑。

### JWT的组成？(自行百度)

由Header（头信息），PayLoad （用户信息）,signature（签名）三个部分组成

Header头信息主要声明加密算法：（具体算法对称不对称加密不作为研究内容）

 通常直接使用 HMAC HS256这样的算法

```json
{
  "typ":"jwt"
  "alg":"HS256" //加密算法
}
```

然后将头部进行base64加密（该加密是可以对称解密的),构成了第一部分.

```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9
```

PayLoad(载荷)

```json
{
  "username":"zhangsan",
  "name":"张三",
  ...
}
```

对其进行base64加密，得到Jwt的第二部分。

```
eyJ1c2VybmFtZSI6InpoYW5nc2FuIiwibmFtZSI6IuW8oOS4iSIsImFnZSI6MTgsInNleCI6IuWlsyIsImV4cCI6MTY0NzE0NTA1MSwianRpIjoiMTIxMjEyMTIxMiJ9
```

Signature 签证信息，这个签证信息由三部分组成（由加密后的Header，加密后的PayLoad，加密后的签名三部分组成）

- header (base64后的)
- payload (base64后的)
- secret

base64加密后的header和base64加密后的payload使用`.`连接组成的字符串，然后通过header中声明的加密方式进行加盐加密，然后就构成了jwt的第三部分，每个部分直接使用"."来进行拼接

```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InpoYW5nc2FuIiwibmFtZSI6IuW8oOS4iSIsImFnZSI6MTgsInNleCI6IuWlsyIsImV4cCI6MTY0NzE0NTA1MSwianRpIjoiMTIxMjEyMTIxMiJ9.5tmHCpcsS_VuZ2_z5Rydf2OpsviBGwB-fJE5aS7gKqE
```







### 如何使用JWT(自行百度)

引入Pom依赖：注：使用的jdk1.8版本 高版本会报缺少jar包

```xml
		<dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt</artifactId>
            <version>0.9.1</version>
        </dependency>
```





#### jwt生成token(自行百度)

调用api封装header，payload，signature这三部分信息

步骤：1什么一个全局的签名即我们的秘钥

```java
static String signature = "itheima";
```

2.获取jwt的构造器

3.封装header，payload，signature这三部分属性

4.调用compact方法帮我们生成token

```java
        //使用JWT自带的构造器构造一个jwt
        JwtBuilder builder = Jwts.builder();
		//使用构造器里的方法封装属性
        String token = builder.
                //封装header属性
                    setHeaderParam("typ","JWT").
                    setHeaderParam("alg", "HS256")
                //封装payload里的信息 使用claim方法
                    .claim("username", "zhangsan")
                    .claim("name", "张三")
                    .claim("age", 18)
                    .claim("sex", "女")
                //在payLoad中设置一个超时时间  秒   分 时
                    .setExpiration(new Date(System.currentTimeMillis()+Long.valueOf(1000 * 60 * 60 * 1)))
                    .setId("1212121212")
                //构造signature部分
                    .signWith(SignatureAlgorithm.HS256, signature)
                //构造我们的签名 调用compact方法
                    .compact();
        System.out.println(token);
```

#### 解密

步骤获取解密器

解密器需要获取我们本地的秘钥 signature

将生成的token进行解密，拿到一个Claims

核心是获取payLoad里的用户信息调用getBody方法获取payLoad

获取payLoad里的参数

```java
		//解密
        JwtParser parser = Jwts.parser();
        Jws<Claims> claimsJws = parser.setSigningKey(signature).parseClaimsJws(token);
        Claims body = claimsJws.getBody();
        //获取name
        System.out.println(body.get("name"));
        //获取性别
        System.out.println(body.get("sex"));
        //获取用户名
        System.out.println(body.get("username"));
        //获取姓名
        System.out.println(body.get("name"));
        //获取id
        System.out.println(body.getId());
        //获取有效期-截止时间
        System.out.println(body.getSubject());
        System.out.println(body.getExpiration());
```

### 完整代码：

```java
package cn.itheima;

import io.jsonwebtoken.*;

import java.util.Date;

/**
 * jwt的例子
 */
public class JWTExample {

    static String signature = "itheima";

    public static void main(String[] args) {
        //使用JWT自带的构造器构造一个jwt
        JwtBuilder builder = Jwts.builder();
        String token = builder.
                //封装header属性
                    setHeaderParam("typ","JWT").
                    setHeaderParam("alg", "HS256")
                //封装payload里的信息 使用claim方法
                    .claim("username", "zhangsan")
                    .claim("name", "张三")
                    .claim("age", 18)
                    .claim("sex", "女")
                //在payLoad中设置一个超时时间  秒   分 时
                    .setExpiration(new Date(System.currentTimeMillis()+Long.valueOf(1000 * 60 * 60 * 1)))
                    .setId("1212121212")
                //构造signature部分
                    .signWith(SignatureAlgorithm.HS256, signature)
                //构造我们的签名 调用compact方法
                    .compact();
        System.out.println(token);
        System.out.println("===================================开始解密=======================================");
        //解密
        JwtParser parser = Jwts.parser();
        Jws<Claims> claimsJws = parser.setSigningKey(signature).parseClaimsJws(token);
        Claims body = claimsJws.getBody();
        //获取name
        System.out.println(body.get("name"));
        //获取性别
        System.out.println(body.get("sex"));
        //获取用户名
        System.out.println(body.get("username"));
        //获取姓名
        System.out.println(body.get("name"));
        //获取id
        System.out.println(body.getId());
        //获取有效期-截止时间
        System.out.println(body.getSubject());
        System.out.println(body.getExpiration());
    }
}
```



















# 4-4 接口资源鉴权

**在看接口资源鉴权这部分代码的时候需要带着一些问题来看代码**

- **1.什么是接口资源**？

- **2.什么是接口资源鉴权**

- **3.如何配置接口资源**
- **4.系统中的接口资源是和什么绑定的（用户，角色，部门？）**

- **5.CRM系统架构中是如何实现接口资源鉴权的**

### **思路**

1.先了解什么是接口资源(百度)

2.了解什么是接口资源鉴权(百度)

3.了解接口资源是和什么绑定的（查看产品原型或问产品经理（老师）），了解需求的过程可以询问产品经理

4.如果自己要实现接口资源鉴权如何实现

5.CRM系统中的接口资源鉴权是如何实现的





### 什么是接口资源？API接口

用户哪些接口是能看到的，有哪些接口是能访问的，那么这些用户能访问的接口就称之为这个用户的接口资源，如果该用户没有对应的接口，后端应该返回403（没有权限）



> 如果我要实现接口资源鉴权，对应的我应该知道每个用户具有哪些资源，这些数据是配置的还是存储在数据库中的，如何进行配置，如何修改，然后前后端是如何交互的?



### 页面配置

1.系统中添加资源

系统管理--权限管理--菜单管理

![](assets/20220313140759.png)

可以配置对应的目录，目录下对应的接口

如果后续添加了新的功能，可以在这里配置目录和接口

![](assets/b2ca0aa8eb1d045f229dbf91fa34c6c.png)

2.角色配置资源

在系统管理--权限管理--角色管理目录下可以点击对应角色的修改

![](assets/20220313140423.png)

点击修改可以修改该角色拥有的接口资源

![](assets/ed95e41ed6d5e4746a595c898e27917.png)

注意：这里的权限标识符需要和代码中的权限标识符进行对应

![](assets/721d14e03795ede4b90476b82e47f54.png)

可以添加对应的资源目录

### 关联数据库表

目录表：sys_menu

![](assets/cfffa0d33b3f701a701b9e61768bbf6.png)

角色表：sys_role

![](assets/ff644cec8adc8b6c074b083e5a093c4.png)

角色和目录关联表：sys_role_menu

![](assets/508a4c91639320a771a47cd11664940.png)

### 具体实现：

#### 前端获取接口资源

首先在登录的时候回应该返回给前端这个用户具有哪些接口资源

这里很多同学会进行猜测，登录的时候，返回的，我们是做技术的，不要盲目猜测，通过前端，通过浏览器的F12看一下就知道这部分接口资源是如何获取的了

我们知道我们在登录的时候主要返回的是token，而token中并没有保存这部分接口资源信息

那么这部分的信息应该什么时候获取呢？

我们查看登录页面登录一次

![](assets/ed0a74258a62f9453e1ac7a5e7a6a94.png)

可以看到在调用登录接口后又调用了一个getInfo接口，我们查看一下这个接口

![](assets/5a421954ed5f53e94cb88d6cb44cadf.png)

我们在后端查看一下这个接口/getInfo

SysLoginController

```java
    /**
     * 获取用户信息
     * 
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }
```

首先getInfo接口是在登录接口之后调用的

所以通过request可以获取对应的用户

通过service查询对应的角色和权限集合将其保存在结果集中返回给前端，这样前端就拥有了对应的角色和接口资源,前端会根据对应的权限标识符来判断用户能看到哪些按钮，哪些目录，具体前端怎么判断的作为后端工程师的你不用关心。



#### 后端接口资源鉴权

利用的是Security里的一个注解@PreAuthorize

这个注解是加载Controller层的需要接口权限的接口上的，对于公共的接口人人都可以访问的接口就不需要加这个注解了

```java
	/**
	 * 查询线索管理列表
	 */
	@PreAuthorize("@ss.hasPermi('clues:clue:list')")
	@GetMapping("/list")
	public TableDataInfo list(TbClue tbClue) {
		...
	}
```

在controller层的接口上加上了@PreAuthorize("@ss.hasPermi('clues:clue:list')")这个注解



这个注解里的内容是ss.hasPermi('xxx:xxx:xx')

具体的实现要看PermissionService

PermissionService

```java
package com.huike.framework.web.service;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.huike.common.core.domain.entity.SysRole;
import com.huike.common.core.domain.model.LoginUser;
import com.huike.common.utils.ServletUtils;
import com.huike.common.utils.StringUtils;

/**
 * RuoYi首创 自定义权限实现，ss取自SpringSecurity首字母
 * 
 * 
 */
@Service("ss")
public class PermissionService {
    /** 所有权限标识 */
    private static final String ALL_PERMISSION = "*:*:*";

    /** 管理员角色权限标识 */
    private static final String SUPER_ADMIN = "admin";

    private static final String ROLE_DELIMETER = ",";

    private static final String PERMISSION_DELIMETER = ",";

    @Autowired
    private TokenService tokenService;

    /**
     * 验证用户是否具备某权限
     * 
     * @param permission 权限字符串
     * @return 用户是否具备某权限
     */
    public boolean hasPermi(String permission) {
        if (StringUtils.isEmpty(permission)) {
            return false;
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser) || CollectionUtils.isEmpty(loginUser.getPermissions())) {
            return false;
        }
        return hasPermissions(loginUser.getPermissions(), permission);
    }

    /**
     * 验证用户是否不具备某权限，与 hasPermi逻辑相反
     *
     * @param permission 权限字符串
     * @return 用户是否不具备某权限
     */
    public boolean lacksPermi(String permission)
    {
        return hasPermi(permission) != true;
    }

    /**
     * 验证用户是否具有以下任意一个权限
     *
     * @param permissions 以 PERMISSION_NAMES_DELIMETER 为分隔符的权限列表
     * @return 用户是否具有以下任意一个权限
     */
    public boolean hasAnyPermi(String permissions)
    {
        if (StringUtils.isEmpty(permissions))
        {
            return false;
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser) || CollectionUtils.isEmpty(loginUser.getPermissions()))
        {
            return false;
        }
        Set<String> authorities = loginUser.getPermissions();
        for (String permission : permissions.split(PERMISSION_DELIMETER))
        {
            if (permission != null && hasPermissions(authorities, permission))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断用户是否拥有某个角色
     * 
     * @param role 角色字符串
     * @return 用户是否具备某角色
     */
    public boolean hasRole(String role)
    {
        if (StringUtils.isEmpty(role))
        {
            return false;
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser) || CollectionUtils.isEmpty(loginUser.getUser().getRoles()))
        {
            return false;
        }
        for (SysRole sysRole : loginUser.getUser().getRoles())
        {
            String roleKey = sysRole.getRoleKey();
            if (SUPER_ADMIN.equals(roleKey) || roleKey.equals(StringUtils.trim(role)))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证用户是否不具备某角色，与 isRole逻辑相反。
     *
     * @param role 角色名称
     * @return 用户是否不具备某角色
     */
    public boolean lacksRole(String role)
    {
        return hasRole(role) != true;
    }

    /**
     * 验证用户是否具有以下任意一个角色
     *
     * @param roles 以 ROLE_NAMES_DELIMETER 为分隔符的角色列表
     * @return 用户是否具有以下任意一个角色
     */
    public boolean hasAnyRoles(String roles)
    {
        if (StringUtils.isEmpty(roles))
        {
            return false;
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser) || CollectionUtils.isEmpty(loginUser.getUser().getRoles()))
        {
            return false;
        }
        for (String role : roles.split(ROLE_DELIMETER))
        {
            if (hasRole(role))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否包含权限
     * 
     * @param permissions 权限列表
     * @param permission 权限字符串
     * @return 用户是否具备某权限
     */
    private boolean hasPermissions(Set<String> permissions, String permission)
    {
        return permissions.contains(ALL_PERMISSION) || permissions.contains(StringUtils.trim(permission));
    }
}
```

可以看到的是我们定义了一个Serivice并且声明了一个在IOC容器中的别名叫ss

在@PreAuthorize("@ss.hasPermi('clues:clue:list')")这个注解中的@ss其实就是从ioc容器中找到叫ss的类

调用里面的hasPermi方法

这个方法代码如下

```java
    /**
     * 验证用户是否具备某权限
     * 
     * @param permission 权限字符串
     * @return 用户是否具备某权限
     */
    public boolean hasPermi(String permission) {
        if (StringUtils.isEmpty(permission)) {
            return false;
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser) || CollectionUtils.isEmpty(loginUser.getPermissions())) {
            return false;
        }
        return hasPermissions(loginUser.getPermissions(), permission);
    }

   /**
     * 判断是否包含权限
     * 
     * @param permissions 权限列表
     * @param permission 权限字符串
     * @return 用户是否具备某权限
     */
    private boolean hasPermissions(Set<String> permissions, String permission)
    {
        return permissions.contains(ALL_PERMISSION) || permissions.contains(StringUtils.trim(permission));
    }
```

其中获取用户信息的部分：

TokenService

```java
    /**
     * 获取用户身份信息
     *
     * @return 用户信息
     */
    public LoginUser getLoginUser(HttpServletRequest request) {
        // 获取请求携带的令牌
        String token = getToken(request);
        if (StringUtils.isNotEmpty(token)) {
            Claims claims = parseToken(token);
            // 解析对应的权限以及用户信息
            String uuid = (String) claims.get(Constants.LOGIN_USER_KEY);
            String userKey = getTokenKey(uuid);
            LoginUser user = redisCache.getCacheObject(userKey);
            return user;
        }
        return null;
    }
    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    private String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(header);
        if (StringUtils.isNotEmpty(token) && token.startsWith(Constants.TOKEN_PREFIX))
        {
            token = token.replace(Constants.TOKEN_PREFIX, "");
        }
        return token;
    }
```

根据用户里的权限列表集合和注解里的权限标识符进行比对

```java
hasPermissions(loginUser.getPermissions(), permission);
```

```java
 /**
     * 判断是否包含权限
     * 
     * @param permissions 权限列表
     * @param permission 权限字符串
     * @return 用户是否具备某权限
     */
    private boolean hasPermissions(Set<String> permissions, String permission)
    {
        return permissions.contains(ALL_PERMISSION) || permissions.contains(StringUtils.trim(permission));
    }
```

如果包含了权限标识符则表示用户具有权限



